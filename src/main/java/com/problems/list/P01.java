package com.problems.list;

import static java.util.Optional.ofNullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class P01 {
	/*
	 * Return the Most Frequent Character
	 * 
	 * Write a function that returns the most frequent character in an array of
	 * words. Examples
	 * 
	 * mostFrequentChar(["apple", "bandage", "yodel", "make"]) ➞ ["a", "e"]
	 * 
	 * mostFrequentChar(["music", "madness", "maniac", "motion"]) ➞ ["m"]
	 * 
	 * mostFrequentChar(["the", "hills", "are", "alive", "with", "the", "sound",
	 * "of", "music"]) ➞ ["e", "h", "i"]
	 * 
	 * Notes
	 * 
	 * If multiple characters tie for most frequent, list all of them in
	 * alphabetical order. Words will be in lower case.
	 */
	public static List<String> getMostFrequent(List<String> elems) {
		/*
		 * Convert string in char and group by chars. This generate Input: "test"
		 * freqChars = t -> 't', 't' -- The integer value of char. It's easy to choose
		 * the e -> 'e' s -> 's'
		 */
		Map<Integer, List<Integer>> freqChars = ofNullable(elems).map(Collection::stream).orElseGet(Stream::empty)
				.map(str -> getListOfInt(str)).flatMap(x -> x.stream()).collect(Collectors.groupingBy(x -> x));

		/*
		 * Find the maximum rec of char.
		 */
		Integer mostFrequent = freqChars.values().stream().map(List::size).max(Comparator.naturalOrder()).orElse(0);

		/*
		 * Return all those chars.
		 */
		return freqChars.keySet().stream().filter(x -> mostFrequent.equals(freqChars.get(x).size()))
				.map(x -> String.valueOf((char) x.intValue())).sorted().collect(Collectors.toList());
	}

	private static List<Integer> getListOfInt(String value) {
		return ofNullable(value).map(x -> x.chars()).orElseGet(IntStream::empty).map(x -> (char) x).boxed()
				.collect(Collectors.toList());
	}
}
