package com.problems.list;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;

public class TestP01 {

	@Test
	public void testMostFrequentChars() {
		assertEquals(Arrays.asList("a", "e"), 
				P01.getMostFrequent(Arrays.asList("apple", "bandage", "yodel", "make")));
		
		assertEquals(Arrays.asList("m"),
				P01.getMostFrequent(Arrays.asList("music", "madness", "maniac", "motion")));
		
		assertEquals(Arrays.asList("e", "h", "i"), 
				P01.getMostFrequent(Arrays.asList("the", "hills", "are", "alive", "with", "the", "sound","of", "music")));
	
		assertNotNull(P01.getMostFrequent(null));
		assertEquals(Arrays.asList(), P01.getMostFrequent(null));
		assertEquals(Arrays.asList(), P01.getMostFrequent(Arrays.asList()));
		
		assertThat(P01.getMostFrequent(Arrays.asList("the", "hills", "are", "alive", "with", "the", "sound", "of", "music")), is(Arrays.asList("e", "h", "i")));            
		assertThat(P01.getMostFrequent(Arrays.asList("apple", "bandage", "yodel", "make")), is(Arrays.asList("a", "e")));
		assertThat(P01.getMostFrequent(Arrays.asList("apple", "bandage", "yodel", "make")), is(Arrays.asList("a", "e")));
		assertThat(P01.getMostFrequent(Arrays.asList("music", "madness", "maniac", "motion")), is(Arrays.asList("m")));            
		assertThat(P01.getMostFrequent(Arrays.asList("let", "them", "eat", "cake")), is(Arrays.asList("e")));            
		assertThat(P01.getMostFrequent(Arrays.asList("potion", "master", "professor", "snape")), is(Arrays.asList("o", "s")));                      
		
	}
}

